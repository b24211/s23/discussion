// Objects

/*
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB,
			keyC: valueC
		}
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
}

console.log('Result from creating objects using initializers/literal notation');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function

/*
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = valueA;
			this.keyB = valueB;
		}
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenonvo', 2008);
console.log('Result from creating objects using object constructors:');
console.log(laptop);
console.log(typeof laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Result from creating objects using objects constructors:');
console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Result from creating objects without the new keyword');
console.log(oldLaptop);

// Accessing array objects

let array = [laptop, myLaptop];
console.log(array[0].name);
console.log(array[0]['name']);

// Initializing/Adding/Deleting/Reassigning object properties

let car = {};

// Initializing/Adding object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation');
console.log(car);

// Initializing/Adding object properties using bracket notation

car['manufactureDate'] = 2019;
console.log('Result from adding properties using bracket notation')
console.log(car['manufactureDate']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log(car.manufacture_Date);
console.log(car);


// Deleting object using properties

delete car['manufactureDate'];
console.log('Result from deleting properties');
console.log(car);

// Reassigning object properties

car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties');
console.log(car);

// Object Methods

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}

console.log(person);
console.log('Result from object methods:');
person.talk();

// Adding new methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
}
person.walk();

let friend = {
	firstName:'Joe',
	lastName: 'Smith',
	adress: {
		city: 'Autsin',
		country: 'Texas'
	},
	email: ['joe@mail.com', 'joesmithe@mail.com'],
	introduce: function(){
		console.log('Hello, my name is ' + this.firstName + ' ' + this.lastName);
	}
}
friend.introduce();

// Real world application of objects

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}

// Creates new instances of "Pokemon" object
let pikachu = new Pokemon('Pikachu', 16);
let rattata = new Pokemon('Rattata', 8);
pikachu.tackle(rattata);
rattata.faint();